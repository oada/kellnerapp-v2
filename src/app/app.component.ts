import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SignalRService } from './services/signal-r.service';
import { KellnerInfoService } from './services/kellner-info.service';
import { RegData } from './models/reg-data.model';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit {
  title = 'OADA';
  regdata: RegData = new RegData();

  constructor() { }
  ngOnInit(): void {

  }

}
