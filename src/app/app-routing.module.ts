import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontpageComponent } from './pages/frontpage/frontpage.component';
import { NeueBestellungComponent } from './pages/neue-bestellung/neue-bestellung.component';


const routes: Routes = [
  {path: 'home', component: FrontpageComponent},
  {path: 'neue-Bestellung', component: NeueBestellungComponent},
  {path: '', pathMatch: 'full', component: FrontpageComponent},
  { path: '**', component: FrontpageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
