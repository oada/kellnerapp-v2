import {
  Directive,
  Output,
  EventEmitter,
  HostBinding,
  HostListener
} from '@angular/core';
import { ToastService } from '../services/toast.service';

@Directive({
  selector: '[long-press]'
})
export class LongPress {
  pressing: boolean;
  longPressing: boolean;
  timeout: any;
  interval: any;
  timer: number = 2;
  constructor(private _snack: ToastService) { }
  @Output()
  onLongPress = new EventEmitter();
  @Output() 
  updateLoadingBar = new EventEmitter();
  @Output()
  resetLoadingBar = new EventEmitter();
  @Output()
  onLongPressing = new EventEmitter();

  @HostBinding('class.press')
  get press() { return this.pressing; }

  @HostBinding('class.longpress')
  get longPress() { return this.longPressing; }

  @HostListener('touchstart', ['$event'])
  @HostListener('mousedown', ['$event'])
  // onMouseDown(event) {
  //   this.timer = 2;
  //   this.pressing = true;
  //   this.longPressing = false;
  //   this.timeout = setTimeout(() => {
  //     this.longPressing = true;
  //     this.onLongPress.emit(event);
  //     this.interval = setInterval(() => {
  //       this.onLongPressing.emit(event);
  //       if (this.timer > 0)
  //         this._snack.open(`Bestellung wird abgeschickt in ${this.timer} Sekunden.`, "");
  //       if (this.timer-- === 0)
  //         window.clearInterval(this.interval);

  //     }, 1000);
  //   }, 100);
  // }
  onMouseDown(event) {
   
    // this.timeout = setInterval(() => {
    //   this.updateLoadingBar.emit(event);
    // }, 10);
  this.timer = 0;
   this.pressing = true;
    this.longPressing = false;
        this.timeout = setTimeout(() => {
       this.longPressing = true;
       this.interval = setInterval(() => {
         if (this.timer >= 0)
          this.updateLoadingBar.emit(event);
         if (this.timer++ === 150)
           window.clearInterval(this.interval);
          
       }, 10);
     }, 100);
  }
  @HostListener('touchend')
  @HostListener('mouseup')
  @HostListener('mouseleave')
  endPress() {  
    clearTimeout(this.timeout);
    clearInterval(this.interval);
    this.longPressing = false;
    this.pressing = false;
  this.resetLoadingBar.emit(event);
  }
}
