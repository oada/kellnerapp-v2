import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BestellungsItem, ComparisonBestellungsItems } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class NeueBestellungService {
  public bestellungUpdated$ = new Subject();
  public itemChanged$ = new Subject();

  constructor() { }

  currentBestellung: BestellungsItem[] = [];
  neueBestellung(bestellung) {
    this.bestellungUpdated$.next(bestellung);
  }

  itemChanged(obj:ComparisonBestellungsItems){
    this.itemChanged$.next(obj);
  }

  neueBestellung2(origEle, newEle) {
    this.bestellungUpdated$.next({origEle: origEle, newEle: newEle});
  }

  setBestellung(bestellung){
    this.currentBestellung = [...bestellung];
  }

  getBestellung():BestellungsItem[]{
    return [...this.currentBestellung];
  }

  isItemSame(item: BestellungsItem, item2: BestellungsItem): boolean {
    if (item.guid === item2.guid && item.kommentar === item2.kommentar)
      return true;
    return false;
  }

}
