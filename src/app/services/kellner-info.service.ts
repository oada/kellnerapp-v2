import { Injectable } from '@angular/core';
import { KellnerInfo } from '../models/kellner-info.model';

@Injectable({
  providedIn: 'root'
})
export class KellnerInfoService {
  info: KellnerInfo = new KellnerInfo();
  tischNr: string = ""
  kommentar: string = ""
  kellner: string = ""
  zahler: string = ""
  abgeschickt: boolean = false
  gesperrt: boolean = true;
  constructor() { }

  setKellner(guid: string): void {
    this.kellner = guid;
    if (this.zahler === "") {
      this.zahler = guid;
    }
  }

  getKellner() {
    return this.kellner;
  }

}
