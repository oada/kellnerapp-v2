/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NeueBestellungService } from './neue-bestellung.service';

describe('Service: NeueBestellung', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NeueBestellungService]
    });
  });

  it('should ...', inject([NeueBestellungService], (service: NeueBestellungService) => {
    expect(service).toBeTruthy();
  }));
});
