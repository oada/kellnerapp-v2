import { Injectable } from '@angular/core';
import { KellnerInfo, MenuItem, Debitor } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getKellnerInfo(): KellnerInfo {
    //here is the request for the KellnerInfo
    return {
      name: "Markus Resch",
      umsatz: 1303.5,
      offen: 0,
      gesperrt: false,
      lastBestellungen: [
        {
          number: 4,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Grillstand",
          zahler: "d3525ed3-0917-4092-a8d3-632bade68ac9",
          betrag: 22.5
        },
        {
          number: 1,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "12",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 222.5
        },
        {
          number: 15,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "11",
          zahler: "d3525ed3-0917-4092-a8d3-632bade68ac9",
          betrag: 11.5
        },
        {
          number: 3,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Sonstiges",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 19
        },
        {
          number: 8,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Sonstiges",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 101
        },
        {
          number: 222,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Sonstiges",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 192
        },
        {
          number: 333,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Sonstiges",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 100
        },
        {
          number: 4,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Grillstand",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 22.5
        },
        {
          number: 1,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "12",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 222.5
        },
        {
          number: 15,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "11",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 11.5
        },
        {
          number: 3,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "Sonstiges",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 19
        },
        {
          number: 8,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "15",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 101
        },
        {
          number: 222,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "14",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 192
        },
        {
          number: 333,
          uhrzeit: "2019-06-01T16:59:41.5924136+02:00",
          tischNr: "13",
          zahler: "ebbab6c8-232a-4c31-a56f-0b8307df2b13",
          betrag: 100
        },
      ]
    }
  }



  getDebitoren(): Debitor[] {
    return [{ guid: "d3525ed3-0917-4092-a8d3-632bade68ac9", name: "Bar" }, { guid: "ebbab6c8-232a-4c31-a56f-0b8307df2b13", name: "Personal" }];
  }
}