import { EventEmitter, Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";

import { RegData, BestellInfo, NewBezahler } from '../models/reg-data.model';
import { KellnerInfo } from '../models/kellner-info.model';
import { Debitor } from '../interfaces';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  kInfo = new EventEmitter<KellnerInfo>();
  debitorenInfo = new EventEmitter<Debitor[]>();
  lastBestellung = new EventEmitter<Debitor[]>();

  private registerConnection: signalR.HubConnection
  public kellnerInfo: KellnerInfo;
  private speisekarte = [];
  private debitoren: Debitor[] = [];
  private kellner: string;
  private regData: RegData;
  private bestellInfo: BestellInfo = new BestellInfo();
  private currentBestellung: any;
  private newZahler: NewBezahler = new NewBezahler();
  public aliveChanged = new EventEmitter<boolean>();
  alive: boolean = false
  timerStarted: boolean = false;
  interval: NodeJS.Timer;

  private url: string = 'http://' + window.location.host
  constructor(private http: HttpClient) {
    console.log("constructor signalRService");
    this.debitoren = [];
    // var debitor = new Debitor();
    // debitor.guid = this.kellner;
    // debitor.name = "Bar";
    // this.debitoren.push(debitor);
    this.registerConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.url + '/register')
      .build();
    this.registerConnection
      .onclose(() => this.connect())


    this.addRegisterDataListener();
    this.addSpeisekartenListener();
    this.addDebitorenListener();
    this.addcurrentBestellungListener();

  }


  public startRegisterConnection = (data: RegData) => {

   // console.log(this.url);
    this.regData = data;
    this.register(this.regData);
    this.requestSpeisekarte();
    this.requestDebitoren();
    this.connect();
  }
  restart() {
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      this.connect();
    }, 5000)
  }

  connect() {
    clearInterval(this.interval);
    if (this.registerConnection.state === signalR.HubConnectionState.Connected) {
      return;
    }
 //   console.log("Connecting");
    this.alive = false;
    this.aliveChanged.emit(this.alive);
    this.registerConnection
      .start()
      .then(() => {
       // console.log('connected');
        this.alive = true;
        this.register(this.regData);
        this.requestSpeisekarte();
        this.requestDebitoren();
        this.aliveChanged.emit(this.alive);
      })
      .catch(err => {
      //  console.log('Error while starting registerConnection: ' + err);
        this.restart()
      })


  }

  private register = (data: RegData) => {
    if (!this.alive) return;
   // console.log("KellnerRegister");
    this.registerConnection.send('KellnerRegister', data)
      // .then(()=>this.registerConnection.on('back', (data) => {
      //   console.log("received "+ data.name);
      // }))
      .catch(err => console.error(err));
  }

  public setNewZahler(nummer: number, newZahler: string) {
    this.newZahler.nummer = nummer;
    this.newZahler.newZahler = newZahler;
    if (!this.alive) return false;
    var ret: boolean = true;
   // console.log(nummer, newZahler);
    this.http.post<any>(`api/Kellner/ChangeZahler/${nummer}`, newZahler).subscribe((data) => {

    //  console.log("Data from HttpRequest", data);
    });
    this.registerConnection.send("ChangeZahler", this.newZahler)
      .catch(err => { ret = false; })
    return ret;
  }

  public getLastKellner(): void {
    this.kInfo.emit(this.kellnerInfo);
  }

  public getLastDebitoren(): void {
    this.registerConnection.send('getDebitoren');
  }

  public addRegisterDataListener = () => {
    this.registerConnection.on('KellnerInfo', (data) => {
      this.kellnerInfo = data;
      this.kInfo.emit(data);
     // console.log(data);
     // console.log("KellnerInfo " + data.name);
    })
  }
  public requestSpeisekarte() {
    if (!this.alive) return;
    this.registerConnection.send('getSpeisekarte');
    this.http.get<any>(`api/Kellner/Speisekarte`).subscribe((data) => {
     // console.log("Data from HttpRequest", data);
    });

  }
  public requestDebitoren() {
    if (!this.alive) return;
    this.registerConnection.send('getDebitoren');

  }
  public addSpeisekartenListener = () => {
    this.registerConnection.on('Speisekarte', (data) => {
      this.speisekarte = data;
      // this.speisekarte = new Array<Produkt>();
      // data.forEach(function (value)
      // {
      //   var produkt = new Produkt(value);
      //   this.speisekarte.push(produkt);
      //   console.log(value);
     // console.log("SPEISEKARTE: ", data);
    }
    )
    //console.log("Speisekarte received "+ this.speisekarte[0].name);

  }
  public addDebitorenListener = () => {
    this.registerConnection.on('Debitoren', (data) => {
      this.debitoren = [];
      const debitor: Debitor = { guid: this.kellner, name: "Bar" };
      this.debitoren.push(debitor);
    //  console.log("debis: ", this.debitoren);
      data.forEach(debitor => {
        this.debitoren.push(debitor);
      });
      this.debitorenInfo.emit(this.debitoren);

    //  console.log("DEBITOREN: ", data);
    })
  }
  public addcurrentBestellungListener = () => {
    this.registerConnection.on('BestellInfo', (data) => {
      this.currentBestellung = data;
      this.lastBestellung.emit(data);

     // console.log("BESTELLINFO: ", data);
    })
  }

  public sendBestellung(bestellung) {
    //neue guid erzeugen
    if (!this.alive) return false;
    var ret: boolean = true;
   // console.log(bestellung);
    this.registerConnection.send("bestellung", bestellung)

      .catch(err => { ret = false; })

    this.http.post<any>(`api/Kellner/Bestellung/`, bestellung).subscribe((data) => {
    //  console.log("Data from HttpRequest", data);
    });

    return ret;
  }

  getProdukte(): any {
  //  console.log("SPEISEKA: ", this.speisekarte);
    return this.speisekarte.slice();
  }

  getDebitoren() {
    return this.debitoren.slice();
  }

  SetKellner(guid: string) {
    this.kellner = guid;
  }

  public getKellner() {
    return this.kellner;
  }

  public getBestellung(nummer: number) {
    var guid = "";
    this.bestellInfo.nummer = nummer;
    this.registerConnection.send('getBestellung', this.bestellInfo)
      .catch(err => console.error(err));
    this.http.get<any>(`api/Kellner/Bestellung/${guid}`).subscribe((data) => {
     // console.log("BESTELLUNG: ", data);
    });

    return this.currentBestellung;
  }
}