/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { KellnerInfoService } from './kellner-info.service';

describe('Service: KellnerInfo', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KellnerInfoService]
    });
  });

  it('should ...', inject([KellnerInfoService], (service: KellnerInfoService) => {
    expect(service).toBeTruthy();
  }));
});
