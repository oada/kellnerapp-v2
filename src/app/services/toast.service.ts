import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private _snackBar: MatSnackBar) { }

  open(message: string, action: string, showTime: number = 2200): void {

    this._snackBar.open(message, action, {
      duration: showTime,
      verticalPosition: 'top'
    });
  }

  openBottom(message: string, action: string, showTime: number = 2200): void {

    this._snackBar.open(message, action, {
      duration: showTime,
      verticalPosition: 'bottom'
    });
  }

  openSuccess(message: string, action: string, showTime: number = 2200): void {
    this._snackBar.open(message, action, {
      duration: showTime,
      verticalPosition: 'top',
      panelClass: ['success-toast']
    });
  }

  openFail(message: string, action: string, showTime: number = 2200): void {
    this._snackBar.open(message, action, {
      duration: showTime,
      verticalPosition: 'top',
      panelClass: ['fail-toast']
    });
  }
}
