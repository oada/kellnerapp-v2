import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { DataService } from 'src/app/services/data-service.service';
import { KellnerInfo, Debitor } from 'src/app/interfaces';
import { ToastService } from 'src/app/services/toast.service';
import { RegData, NewBezahler } from 'src/app/models/reg-data.model';
import { ActivatedRoute } from '@angular/router';
import { SignalRService } from 'src/app/services/signal-r.service';
import { KellnerInfoService } from 'src/app/services/kellner-info.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';


@Component({
  // selector: 'frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.scss']
})

export class FrontpageComponent implements OnInit, OnDestroy {
  currentKellnerInfo: KellnerInfo = {};
  panelOpenState: boolean = false;
  currentDebitoren: Debitor[];
  regdata: RegData = new RegData();
  // subscription: Subscription;
  //debitoren$: Subscription;
  info: KellnerInfo;



  constructor(
    private _data: DataService,
    private _toast: ToastService,
    private route: ActivatedRoute,
    private SignalRService: SignalRService,
    private _kellner: KellnerInfoService,
    public dialog: MatDialog,
    private http: HttpClient) {
    this.route.queryParams.subscribe(params => {
      var param = params['Guid'];
      if (param != null) {
        this.regdata.guid = param;
        this.regdata.password = params['Password'];
        window.localStorage.setItem("userguid", this.regdata.guid);
        window.localStorage.setItem("password", this.regdata.password);
      }
      else {
        this.regdata.guid = window.localStorage.getItem("userguid");
        this.regdata.password = window.localStorage.getItem("password");;
      }
      this._kellner.setKellner(this.regdata.guid);

      // this.SignalRService.SetKellner(this.regdata.guid);
      // this.SignalRService.startRegisterConnection(this.regdata);
    });
  }
  testSub: Subscription;

  KellnerInfoHttpTest;
  ngOnInit() {
    this.http.get<any>(`api/Kellner/Info/${this.regdata.guid}`).subscribe((info) => {
      info.lastBestellungen.sort(function (a, b) {
        return Date.parse(b.uhrzeit) - Date.parse(a.uhrzeit);
      });
      this.currentKellnerInfo = info;
     // console.log("KELLNERINFO FROM HTTP: ", info);
    });
    this.http.get<any>(`api/Kellner/Debitoren/${this.regdata.guid}`).subscribe((data) => {
      //this.KellnerInfoHttpTest = data;
      this.currentDebitoren = JSON.parse(JSON.stringify(data));
    //  console.log("DEBITOREN FROM HTTP: ", this.currentDebitoren);
    });

    // this.subscription = this.SignalRService.kInfo
    //   .subscribe
    //   (
    //     (info: KellnerInfo) => {
    //       console.log("Kellnerinfo: ")
    //       console.log(info);
    //       //sort
    //       info.lastBestellungen.sort(function (a, b) {
    //         return Date.parse(b.uhrzeit) - Date.parse(a.uhrzeit);
    //       });
    //       this.currentKellnerInfo = info;
    //       //this.currentKellnerInfo.gesperrt = true;
    //       //fills the first BestellInfo
    //       //  this.getLastBestellung();

    //       // this.transformStringsToDates(this.currentKellnerInfo);
    //       // this.bpService.SetGesperrt(info.gesperrt);
    //     }
    //   );

    // this.debitoren$ = this.SignalRService.debitorenInfo.subscribe(
    //   (debitoren: Debitor[]) => {
    //     this.currentDebitoren = JSON.parse(JSON.stringify(debitoren));
    //     // this.transformStringsToDates(this.currentKellnerInfo);

    //     // this.bpService.SetGesperrt(info.gesperrt);
    //   }
    // )
    // this.currentKellnerInfo = this._data.getKellnerInfo();
    // this.testSub = this.SignalRService.lastBestellung.subscribe(
    //   (data) => {
    //     let tempObj = { ...data };
    //     //data.auslieferungsZeit = Date.parse(data.auslieferungsZeit);
    //     console.log("JSADUIHAFDHGBSDHFSDFSDFHBSDHBFSDHBFHSBDFHBSDHFSHDBFHFSDDFH");
    //     this.currentBestellung = null;
    //     this.currentBestellung = JSON.parse(JSON.stringify(this.combineSameItems(tempObj)));
    //     // this.bpService.SetGesperrt(info.gesperrt);
    //   }
    // )

  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
    //this.debitoren$.unsubscribe();
  }

  showBestellungDetails(bestellung): void {
   // console.log("show me the Details for: ", bestellung);

    const dialogRef = this.dialog.open(DetailDialog, {
      minHeight: '90%',
      maxHeight: '90%',
      width: '90% !important',
      minWidth: "90%",
      position: { top: '12px' },
      data: {
        bestellung: bestellung,
        debitoren: this.currentDebitoren,
        currentKellnerInfo: this.currentKellnerInfo
      }
    });
  }


  currentBestellung: any;
  getBestellung(bestellungsnr: number): void {
    this.currentBestellung = null;
    this.SignalRService.getBestellung(bestellungsnr);

  }

  getLastBestellung() {
    this.getBestellung(this.currentKellnerInfo.lastBestellungen[0].number);
  }

  transformZahlerToDebitor(obj: KellnerInfo, debitoren: Debitor[]): KellnerInfo {
    obj.lastBestellungen.forEach(bestellung => {
      for (let i = 0; i < debitoren.length; i++) {
        const debitor = debitoren[i];
        if (debitor.guid === bestellung.zahler) {
          bestellung.zahler = { ...debitor };
        }
      }
    });
   // console.log("transformed obj: ", obj);

    return obj;
  }
  sendNewBezahler(event, bestellung): void {
    //here we need to send the newBezahler
  //  console.log("switched!", event);
  //  console.log("switched!", bestellung);
    this.SignalRService.setNewZahler(bestellung.number, bestellung.zahler);
    //if success

    this._toast.open(`Bezahlung für Tisch ${bestellung.tischNr} aktualisiert!`, 'OK');
  }
}

@Component({
  selector: 'detail-view-dialog',
  templateUrl: 'detail-view-dialog.html',
})
export class DetailDialog implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, private _toast: ToastService, ) { }
  bestellungDetails: any;
  private newZahler: NewBezahler = new NewBezahler();
  isBestellungReceived: boolean = false;

  ngOnInit(): void {
   // console.log("DETAILS: ", this.data);
    //HIER IST DER FEHLER
    this.http.get<any>(`api/Kellner/Bestellung/${this.data.bestellung.bestellGuid}`).subscribe((bestellung) => {
      //console.log("BESTELLUNG: ", bestellung);
      this.isBestellungReceived = true;
      this.bestellungDetails = JSON.parse(JSON.stringify(this.combineSameItems(bestellung)));
      //  this.bestellungDetails = bestellung;
    });
  }

  tempData: any = [];
  combineSameItems(data) {
    this.tempData = [];

    for (let i = 0; i < data.bestellListe.length; i++) {
      const ele = data.bestellListe[i];
      ele['anzahl'] = 1;
      this.isNotInArr(ele, this.tempData);

    }
    data.bestellListe = JSON.parse(JSON.stringify(this.tempData));

    return JSON.parse(JSON.stringify(data));
  }

  isNotInArr(item, arr): void {
    for (let i = 0; i < arr.length; i++) {
      let element = arr[i];
      if (item.zusatzInfo === element.zusatzInfo && item.name === element.name) {
        element.anzahl = element.anzahl + 1;
        return;
      }
    }
    arr.push(JSON.parse(JSON.stringify(item)));
  }

  setNewBezahler(event): void {
    this.newZahler.newZahler = event.value;
    //HTTP ERROR: 400 (Bad Request)
    this.http.post<any>(`api/Kellner/ChangeZahler/${this.data.bestellung.bestellGuid}`, this.newZahler).subscribe((data) => {
    //  console.log("NEUER BEZAHLER:", data);
      this._toast.open(`Bezahlung für Tisch ${this.data.bestellung.tischNr} aktualisiert!`, 'OK');
    });

    //HTTP ERROR
    // this.http.post<any>(`api/Kellner/ChangeZahler/${this.data.bestellung.bestellGuid}`, JSON.stringify(this.newZahler)).subscribe((data) => {
    //   console.log("NEUER BEZAHLER:", data);
    //   this._toast.open(`Bezahlung für Tisch ${this.data.bestellung.tischNr} aktualisiert!`, 'OK');
    // });
  }

  cancelDetailView(): void {

  }
}