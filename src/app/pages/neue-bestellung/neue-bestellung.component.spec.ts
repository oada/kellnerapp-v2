/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NeueBestellungComponent } from './neue-bestellung.component';

describe('NeueBestellungComponent', () => {
  let component: NeueBestellungComponent;
  let fixture: ComponentFixture<NeueBestellungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeueBestellungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeueBestellungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
