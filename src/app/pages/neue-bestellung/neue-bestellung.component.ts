import { Component, OnInit, ViewEncapsulation, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data-service.service';
import { MenuItem, NeueBestellung, BestellungsItem, NeueBestellungToSend, BestellungsItemToSend, ComparisonBestellungsItems } from 'src/app/interfaces';
import { trigger, transition, animate, keyframes, state, style } from '@angular/animations';
import * as kf from 'src/app/animations/keyframes';
import { interval, Observable, of } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { SignalRService } from 'src/app/services/signal-r.service';
import { NeueBestellungService } from 'src/app/services/neue-bestellung.service';
import { Guid } from "guid-typescript";
import { HttpClient } from '@angular/common/http';
import { KellnerInfoService } from 'src/app/services/kellner-info.service';
import { rowsAnimation, fadeSlideInOut } from "src/app/animations/keyframes";
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'neue-bestellung',
  templateUrl: './neue-bestellung.component.html',
  styleUrls: ['./neue-bestellung.component.css'],
  animations: [
    trigger('cardAnimator', [
      transition('* => swiperight', animate(200, keyframes(kf.swiperight))),
      transition('* => swipeleft', animate(200, keyframes(kf.swipeleft)))
    ]),
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    [rowsAnimation, fadeSlideInOut]
  ],
  encapsulation: ViewEncapsulation.None

})
export class NeueBestellungComponent implements OnInit {
  indexOfNewOrder: number = 0;
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  currentMenu: MenuItem[];
  neueBestellung: NeueBestellung = { tischNr: '', kommentar: '', bestellung: [] };
  arrOfMenuParts = [];
  dataSource: MatTableDataSource<BestellungsItem> = new MatTableDataSource<BestellungsItem>();

  expandedElement: any | null;
  public index = 0;
  public maxIndex = 0;
  animationState: string;
  displayedColumns = ['name', 'kommentar', 'preis'];

  getTotalCost() {
    let totalCost: number = 0;
    this.dataSource.data.forEach(item => {
      totalCost += (item.preis * item.anzahl);
    });
    return totalCost;
  }

  startAnimation(state) {
   // console.log(state);
    if (state === 'swiperight' && this.index > 0 && !this.animationState) {
      this.index--;
      this.animationState = state;
    } else if (state === 'swipeleft' && this.index < this.maxIndex && !this.animationState) {
      this.index++;
      this.animationState = state;
    }
  }

  resetAnimationState(state) {
    this.animationState = '';
  }

  @ViewChild('button') button: ElementRef;
  @ViewChild('scrollMe') commentList: ElementRef;
  longPressing = 1;

  onLongPress() {
    this.longPressing = null;
  }
  progressbarValue = 0;
  curSec: number = 0;

  startTimer(seconds: number) {
    const time = seconds;
    const timer$ = interval(1000);

    const sub = timer$.subscribe((sec) => {
      this.progressbarValue = 100 - sec * 100 / seconds;
      this.curSec = sec;

      if (this.curSec === seconds) {
        sub.unsubscribe();
      }
    });
  }
  updateLoadingBar(){
    const max:number = 145;
    const min:number = 0;
    if (this.dataSource.data.length > min) {
      if (this.progressbarValue === max) {
        this.sendBestellung();
      }
    this.progressbarValue++;
    }
  }

  resetLoadingBar(){
    this.progressbarValue = 0;
  }

  onLongPressing() {
    this.longPressing++;
    if (this.dataSource.data.length > 0) {
      if (this.longPressing > 2) {
        // this._toast.openSuccess("Bestellung abgesendet!", "Ok", 1000);
        this.sendBestellung();
      }
    } else {
      this._toast.open("Eine leere Bestellung kann nicht abgesendet werden!", "");
    }
  }

  showBestellungsInfo(): void {
    this._toast.open("Bitte den Bestellbutton 3 Sekunden gedrückt halten um abzusenden.", "");
  }

  constructor(
    private _toast: ToastService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private _data: DataService,
    private changeDetectorRef: ChangeDetectorRef,
    private _kellner: KellnerInfoService,
    private SignalRService: SignalRService,
    private http: HttpClient,
    private _neueBestellung: NeueBestellungService,
    private _neueBestellung2: NeueBestellungService) { }

  ngOnInit() {
    this.getCurrentMenu();
    this.firstFormGroup = this._formBuilder.group({
      tischNr: ['', [Validators.required, Validators.maxLength(7)]],
      kommentar: ['']
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this._neueBestellung.bestellungUpdated$.subscribe((bestellung) => {
    //  console.log("SUBSCRIPTION_TRIGGER", bestellung);
      this.dataSource.data.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
    });

    this._neueBestellung.itemChanged$.subscribe((obj: ComparisonBestellungsItems) => {
     // console.log("ITEM HAS CHANGED!!!", obj);
      let compObj: ComparisonBestellungsItems = obj;

      if (compObj.changedItem.kommentar !== "" && compObj.changedItem.kommentar !== compObj.originalItem.kommentar){
        for (let i = 0; i < this.dataSource.data.length; i++) {
          let ele = this.dataSource.data[i];
          if (this.isItemSame(ele, compObj.originalItem)) {
           // console.log("found!", ele);
            if (ele.anzahl > 1) {
            //  console.log("anzahl  > 1", ele.anzahl);
              ele.anzahl--;
              compObj.changedItem.anzahl = 1;
              this.dataSource.data = this.dataSource.data.map(x => Object.assign({}, x));
              this.dataSource.data.push({ ...compObj.changedItem });
              //this._neueBestellung.neueBestellung(this.neueBestellung.bestellung);
            //  console.log("ENDGÜLTIGES ARRAY: ", this.dataSource.data);
              this.dataSource.data.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
              this.dataSource = new MatTableDataSource([...this.dataSource.data]);
              break;
            } else if (ele.anzahl === 1) {
              //just change the kommentar from the ref
              ele.kommentar = compObj.changedItem.kommentar;
              break;
            }
          }
        }
      } else if(compObj.changedItem.kommentar === ""){
        //kommentar von ehemals befülltem ist jetzt empty
        for (let i = 0; i < this.dataSource.data.length; i++) {
          let ele = this.dataSource.data[i];
          if (this.isItemSame(ele, compObj.originalItem)) {
            if (ele.anzahl > 1) {
              ele.anzahl--;
              ele.kommentar = compObj.changedItem.kommentar;
              break;
            } else if (ele.anzahl === 1) {
              //just change the kommentar from the ref
              ele.kommentar = compObj.changedItem.kommentar;
              break;
            }
          }
        }
      }




    })
  }

  sendBestellung(): void {
    //wenn request erfolgreich beantwortet, dann schicken wir wieder auf /home zurück
    this.breakBestellungIntoParts();
    const newGuid = Guid.create();
    var bestellung: NeueBestellungToSend =
    {
      tischNr: this.neueBestellung.tischNr,
      kommentar: this.neueBestellung.kommentar,
      kellner: this._kellner.getKellner(),
      zahler: this._kellner.getKellner(),
      betrag: 0,
      bestellGuid: newGuid.toString(), // new GUID
      speisen: new Array<BestellungsItemToSend>()
    }
   // console.log("NEUE BESTELLUNG MIT GUID: ", bestellung);
    this.dataSource.data.forEach(function (value) {
      var item: BestellungsItemToSend =
      {
        speise: value.guid,
        kommentar: value.kommentar,
        preis: value.preis
      }
      bestellung.betrag += value.preis;
      bestellung.speisen.push(item);
    }, this)

    this.http.post<any>(`api/Kellner/Bestellung/`, bestellung).subscribe(
      (data) => {
      //  console.log("Data from HttpRequest", data);
        if (data.success === true) {
          this.router.navigate(['home']);
          this._toast.openSuccess(data.comment, '');
        } else {
          this._toast.openFail(data.comment, '');
        }
      });
  }

  breakBestellungIntoParts() {
    let tempBestellung = [];
    this.dataSource.data.forEach(elem => {
      for (let i = 0; i < elem.anzahl; i++) {
        let tempOrder = { name: elem.name, preis: elem.preis, farbe: elem.farbe, guid: elem.guid, kommentar: elem.kommentar };
        tempBestellung.push(JSON.parse(JSON.stringify(tempOrder)));
      }
    });
    this.dataSource.data = JSON.parse(JSON.stringify(tempBestellung));
  //  console.log("tempBestellung: ", tempBestellung);
  }

  getCurrentMenu(): void {
    //here we get the currentMenuList and init our member
    this.http.get<any>(`api/Kellner/Speisekarte`).subscribe((speisekarte) => {
      this.currentMenu = speisekarte.slice();
      this.currentMenu.forEach(elem => {
        elem['kommentar'] = '';
      });
      // this.currentMenu[0].gesperrt = true;
      // this.currentMenu[1].gesperrt = true;
      // this.currentMenu[2].gesperrt = true;

      this.breakMenuIntoParts(this.currentMenu, this.arrOfMenuParts);
    //  console.log(this.arrOfMenuParts);
    });

  }

  breakMenuIntoParts(arr: MenuItem[], arrayOfMenu: any): any {
    let tempArr = [];
   // console.log("CURRMENU: ", this.currentMenu);

    var i, j, chunk = 12;
    for (i = 0, j = arr.length; i < j; i += chunk) {
      tempArr = arr.slice(i, i + chunk);
      arrayOfMenu.push([...tempArr]);
      tempArr = [];
    }
    this.maxIndex = arrayOfMenu.length - 1;
  }
  scrollDown: any;
  bestelle(item): void {
  //  console.log(item);
    // if (item.kommentar)
    //   this._toast.openBottom(`${item.name} (${item.kommentar}) hinzugefügt!`, '', 700);
    // else
    //   this._toast.openBottom(`${item.name} hinzugefügt!`, '', 700);

    this.addItem(item);
    // HIER ÜBERLEGEN
   // this.dataSource.data = this.dataSource.data.map(x => Object.assign({}, x));
    this.neueBestellung = { ...this.neueBestellung };
   // this.changeDetectorRef.detectChanges();
    //this.dataSource.data.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
    //this.dataSource = new MatTableDataSource([...this.dataSource.data]);
    //elementref scrolldown here
    this.scrollDown = this.commentList.nativeElement.scrollHeight;

  }
 

  addItem(newItem): void {
    if (this.dataSource.data.length === 0) {
    //  this.dataSource.data.push({ ...newItem, kommentar: '', anzahl: 1 });
      this.dataSource.data.push({ ...newItem, kommentar: '', anzahl: 1 });
      this.dataSource.data = this.dataSource.data.map(x => Object.assign({}, x));
      this.neueBestellung.bestellung = this.dataSource.data;
      this._neueBestellung.setBestellung(this.dataSource.data);
      return;
    }
    for (let i = 0; i < this.dataSource.data.length; i++) {
     // let item = this.dataSource.data[i];
      if (this.isItemSame(this.dataSource.data[i], newItem)) {
        this.dataSource.data[i].anzahl++;
        this.neueBestellung.bestellung = this.dataSource.data;
        this._neueBestellung.setBestellung(this.dataSource.data);
        return;
      }
    }
    this.dataSource.data.push({ ...newItem, kommentar: '', anzahl: 1 });
    this.neueBestellung.bestellung = this.dataSource.data;
   // this.dataSource.data.push({ ...newItem, kommentar: '', anzahl: 1 });
    this.dataSource.filter = "";
    this._neueBestellung.setBestellung(this.dataSource.data);

  }

  removeItem(newItem): void {
    for (let i = 0; i < this.dataSource.data.length; i++) {
      let item = this.dataSource.data[i];
      if (this.isItemSame(item, newItem)) {
        if (item.anzahl > 1) {
          // if (item.kommentar)
          //   this._toast.open(`${item.name} (${item.kommentar}) entfernt!`, '');
          // else
          //   this._toast.open(`${item.name} entfernt!`, '');
          item.anzahl--;
          this.dataSource = new MatTableDataSource([...this.dataSource.data]);
          this.neueBestellung.bestellung = [...this.dataSource.data];
          this._neueBestellung.setBestellung(this.dataSource.data);
          return;
        } else {
          // if (item.kommentar)
          //   this._toast.open(`${item.name} (${item.kommentar}) entfernt!`, '');
          // else
          //   this._toast.open(`${item.name} entfernt!`, '');
          this.dataSource.data.splice(i, 1);
         // this.dataSource = new MatTableDataSource([...this.dataSource.data]);
         this.dataSource = new MatTableDataSource([...this.dataSource.data]);
         this.neueBestellung.bestellung =[...this.dataSource.data];
         this._neueBestellung.setBestellung(this.dataSource.data);
          return;
        }

      }
    }
  }

  isItemSame(item: BestellungsItem, item2: BestellungsItem): boolean {
    if (item.guid === item2.guid && item.kommentar === item2.kommentar)
      return true;
    return false;
  }

  addTischNr(): void {
    this.neueBestellung.tischNr = this.firstFormGroup.get('tischNr').value;
    this.neueBestellung.kommentar = this.firstFormGroup.get('kommentar').value;
  }

  bestellungAbbrechen() {
    this._toast.open("Bestellung wurde abgebrochen!", "");
  }

  hideKeyboard(element) {
    element.setAttribute('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.setAttribute('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function () {
      element.blur();  //actually close the keyboard
      // Remove readonly attribute after keyboard is hidden.
      element.removeAttribute('readonly');
      element.removeAttribute('disabled');
    }, 100);
  }

  closeKeyboard(ev) {
    this.hideKeyboard(ev.srcElement);
  }
}


