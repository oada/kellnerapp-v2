export class KellnerInfo {
    public name: string;
    public umsatz: number;
    public offen: number;
    public lastBetrag: number;
    public lastTischNr: string;
    public lastBestellung: Date;
    public gesperrt: boolean;
}