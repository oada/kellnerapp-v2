export class RegData {
    public guid: string;
    public password: string;
    public name: string;
}

export class BestellInfo {
    public nummer: number;
}

export class NewBezahler {
    public nummer: number;
    public newZahler: string;
}