import { Guid } from 'guid-typescript';

export interface KellnerInfo {
  name?: string,
  umsatz?: number,
  offen?: number,
  gesperrt?: boolean,
  lastBestellungen?: Bestellungen[]
}

export interface Bestellungen {
  number?: number,
  uhrzeit: string,
  tischNr: string,
  zahler: string | Debitor,
  betrag: number,
  bestellGuid?: string
}


export interface MenuItem {
  name: string,
  preis: number,
  farbe: string,
  guid: string,
  gesperrt: boolean
}

export interface Debitor {
  guid: string,
  name: string
}

export interface NeueBestellung {
  tischNr: string;
  kommentar?: string;
  bestellung: BestellungsItem[];
}

export interface BestellungsItem {
  name: string,
  kommentar?: string,
  guid: string,
  anzahl: number,
  preis: number,
  farbe: string
}

export interface NeueBestellungToSend {
  tischNr: string;
  kommentar: string;
  kellner: string;
  zahler: string;
  betrag: number;
  bestellGuid: any;
  speisen: BestellungsItemToSend[],
}

export interface BestellungsItemToSend {
  speise: string;
  kommentar: string;
  preis: number;
}


export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface ComparisonBestellungsItems {
  originalItem: BestellungsItem;
  changedItem: BestellungsItem;
}