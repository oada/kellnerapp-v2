/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { KommentarElementComponent } from './kommentar-element.component';

describe('KommentarElementComponent', () => {
  let component: KommentarElementComponent;
  let fixture: ComponentFixture<KommentarElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KommentarElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KommentarElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
