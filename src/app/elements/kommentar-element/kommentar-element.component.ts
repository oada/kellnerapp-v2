import { Component, OnInit, Input, Inject, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastService } from 'src/app/services/toast.service';
import { BestellungsItem } from 'src/app/interfaces';
import { NeueBestellungService } from 'src/app/services/neue-bestellung.service';

@Component({
  selector: 'kommentar-element',
  templateUrl: './kommentar-element.component.html',
  styleUrls: ['./kommentar-element.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class KommentarElementComponent implements OnInit {
  @Input('element') element: any;
  constructor(public dialog: MatDialog, private cdr: ChangeDetectorRef, private _neueBestellung: NeueBestellungService) { }

  aktuelleBestellung: BestellungsItem[] = [];
  origElement: BestellungsItem = { name:"", guid:"", anzahl: 0, preis: 0, farbe:""};
  copyElement: BestellungsItem = { name:"", guid:"", anzahl: 0, preis: 0, farbe:""};
  ngOnInit(): void {
    this.aktuelleBestellung = [...this._neueBestellung.getBestellung()];
    this.initOrigElement(this.element);
  }

  private initOrigElement(ele:BestellungsItem){
    this.origElement.name = ele.name;
    this.origElement.guid = ele.guid;
    this.origElement.farbe = ele.farbe;
    this.origElement.preis = ele.preis;
    this.origElement.anzahl = ele.anzahl;
    this.origElement.kommentar = ele.kommentar;
  }

  openComment(): void {
    this.copyElement = {...this.element};
    this.initOrigElement(this.element);
    const dialogRef = this.dialog.open(KommentarDialog, {
      minHeight: '60%',
      width: '100% !important',
      minWidth: "100%",
      position: { top: '0px' },
      data: {
        element: this.copyElement
      }
    });

    dialogRef.beforeClosed().subscribe(result => {
     // console.log(`Dialog result:`, result);
      this._neueBestellung.itemChanged({originalItem: this.origElement, changedItem: this.copyElement});
      })
  }

 

}

@Component({
  selector: 'kommentar-dialog',
  templateUrl: 'kommentar-dialog.html',
})
export class KommentarDialog implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
  origComment: string = "";

  ngOnInit(): void {
    this.origComment = this.data.element.kommentar;
  }

  cancelComment(): void {
    this.data.element.kommentar = this.origComment;
  }

  hideKeyboard(element) {
    element.setAttribute('readonly', 'readonly'); // Force keyboard to hide on input field.
    element.setAttribute('disabled', 'true'); // Force keyboard to hide on textarea field.
    setTimeout(function () {
      element.blur();  //actually close the keyboard
      // Remove readonly attribute after keyboard is hidden.
      element.removeAttribute('readonly');
      element.removeAttribute('disabled');
    }, 100);
  }

  closeKeyboard(ev) {
    this.hideKeyboard(ev.srcElement);
   // console.log("enter pressed");
  }
}